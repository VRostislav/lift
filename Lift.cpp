#include <iostream>
#include <exception>
#include <functional>
#include <limits>

#include "Timer.hpp"
#include "Log.hpp"
#include "Lift.hpp"


#define WAITING_INPUT_TIMEOUT 10
#define READING_BUFFER_LEN 32

namespace chr = std::chrono;


void Lift::run() {
    LOG(TRACE) << "Enter level num > ";
    while (_is_run) {
        /// Получить значение нужного этажа
        if (std::cin.peek() not_eq EOF) { //and std::cin.read(need_level_str, READING_BUFFER_LEN)) {
            try {
                char need_level_str[READING_BUFFER_LEN] = {0};
                std::cin.getline(need_level_str, READING_BUFFER_LEN); 
                int need_level = std::stoi(std::string(need_level_str));

                /// Проверка запрошенного этажа и выполнение сценария работы лифта.
                if (_levels < need_level) {
                    LOG(WARNING) << "Max level is " << _levels << ".";
                    need_level = _levels;
                } else if (2 > need_level) {
                    LOG(WARNING) << "Level must be large 1.";
                    need_level = 1;
                } else {
                    /// Выполнение сценария.
                    movToFirstLevel();
                    openDoor();
                    loadingTimeout();
                    pushLevelButton();
                    closeDoor();
                    movToLevel(need_level);
                    openDoor();
                    openingTimeout();
                    closeDoor();
                } 
            } catch(std::invalid_argument e) {
                if (_is_run) {
                    LOG(WARNING) << "Need level number";
                }
            }
            if (_is_run) {
                LOG(TRACE) << "Enter level num > ";
            }
        }
        std::this_thread::sleep_for(chr::milliseconds(WAITING_INPUT_TIMEOUT));
    }
}


void Lift::movToFirstLevel() {
    if (_is_run and _cur_level not_eq 1) {
        LOG(DEBUG) << "from " << _cur_level;
        movToLevel(1);
    }
}


void Lift::openDoor() {
    if (_is_run) {
        LOG(DEBUG) << _door_timeout << " {";
        _timeout = std::make_shared<Timer>(_door_timeout, [] {});
        _timeout->wait();
        LOG(DEBUG) << "}";
    }
}


void Lift::loadingTimeout() {
    if (_is_run) {
        int loading_timeout = (rand() % (LOADING_TIMEOUT_MAX - LOADING_TIMEOUT_MIN)) + LOADING_TIMEOUT_MIN; 
        LOG(DEBUG) << loading_timeout << " {";
        _timeout = std::make_shared<Timer>(loading_timeout, [] {});
        _timeout->wait();
        LOG(DEBUG)  << "}";
    }
}


void Lift::pushLevelButton() {
    if (_is_run) {
        LOG(DEBUG);
    }
}


void Lift::openingTimeout() {
    if (_is_run) {
        LOG(DEBUG) << OPENING_TIMEOUT << " {";
        _timeout = std::make_shared<Timer>(OPENING_TIMEOUT, [] {});
        _timeout->wait();
        LOG(DEBUG) << "}";
    }
}


void Lift::closeDoor() {
    if (_is_run) {
        LOG(DEBUG) << _door_timeout << " {";
        _timeout = std::make_shared<Timer>(_door_timeout, [] {});
        _timeout->wait();
        LOG(DEBUG) << "}";
    }
}


void Lift::movToLevel(int need_level_) {
    if (_is_run) {
        LOG(DEBUG) << "Level is " << _cur_level << ", need " << need_level_;
        int mov_timeout = _level_height * 1000 / _speed;
        int step = 1;
        if (need_level_ < _cur_level) {
            step = -1;
        }
        do {
            _timeout = std::make_shared<Timer>(mov_timeout, [] {});
            _timeout->wait();
            _cur_level += step;
            LOG(DEBUG) << "Level is " << _cur_level;
        } while (_is_run and _cur_level not_eq need_level_);
    }
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


Lift::Lift(int levels_, int speed_, int level_height_, int door_timeout_) 
    : _levels(levels_)
    , _speed(speed_)
    , _level_height(level_height_)
    , _door_timeout(door_timeout_) 
    , _is_run(false)
    , _cur_level(1) { 
    LOG(DEBUG) << "levels=" <<  levels_ << ", speed=" <<  speed_ 
               << ", level_height=" << level_height_ << ", door_timeout=" << door_timeout_;
    /// Инициализация псевдо случайной функции для вычисления задержки входа пассажира.
    srand(time(nullptr));
}


void Lift::start() {
    LOG(DEBUG);
    _is_run = true;
    _thread = PThread(new Thread(std::bind(&Lift::run, this)), [this](Thread *p_) {
        _is_run = false;
        p_->join();
    });
}


void Lift::stop() {
    LOG(DEBUG);
    _thread.reset();
}
