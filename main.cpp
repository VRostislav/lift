/** Copyright &copy; 2017, rostislav.vel@gmail.com.
 * \brief  Симулятор лифта.
 * \author Величко Ростислав
 * \date   11.12.2017
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include <memory>
#include <iostream>

#include "Log.hpp"
#include "SignalDispatcher.hpp"
#include "Lift.hpp"


#define MIN_LEVELS 5
#define DEFAULT_LEVELS 10
#define DEFAULT_SPEED 1
#define DEFAULT_LEVEL_HEIGHT 2
#define DEFAULT_DOOR_TIMEOUT 3000


struct GlobalArgs {
    int _levels;       /// параметр -l
    int _speed;        /// параметр -s
    int _level_height; /// параметр -h
    int _door_timeout; /// параметр -t
} __global_args;


static const char *__opt_string = "l:s:H:t:h?";
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void HelpMessage() {
    std::cout << "  Use:\n\t#lift -l 10 -s 1 -H 2 -t 3000\n"
              << "  Args:\n"
              << "\t[-l]\t Levels num. [5 <-> 20]. Default: " << DEFAULT_LEVELS << "\n"
              << "\t[-s]\t Moving speed. [1 <-> 10]. Default: " << DEFAULT_SPEED << "\n"
              << "\t[-H]\t Level height in meters. [2 <-> 4]. Default: " << DEFAULT_LEVEL_HEIGHT << "\n"
              << "\t[-t]\t Open/Close door timeout in mlsec. [100 <-> 10000]. Default: " << DEFAULT_DOOR_TIMEOUT << "\n"
              << "__________________________________________________________________\n\n";
    exit(EXIT_FAILURE);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


typedef std::shared_ptr<Lift> PLift;
typedef utils::SignalDispatcher SignalDispatcher;
typedef std::shared_ptr<SignalDispatcher> PSignalDispatcher;


int main(int argc_, char **argv_) {
    LOG_TO_STDOUT;
    int opt = 0;

    /// Инициализация globalArgs до начала работы с ней.
    __global_args._levels = DEFAULT_LEVELS;      
    __global_args._speed = DEFAULT_SPEED;       
    __global_args._level_height = DEFAULT_LEVEL_HEIGHT;
    __global_args._door_timeout = DEFAULT_DOOR_TIMEOUT;

    /// Обработка входных опций.
    opt = getopt(argc_, argv_, __opt_string);
    while(opt != -1) {
        int value = 0;
        switch(opt) {
            case 'l':
                value = strtol(optarg, (char**)NULL, 10);
                if (not value or (MIN_LEVELS > value) or (20 < value)) {
                    HelpMessage();
                }
                __global_args._levels = value;
                break;
            case 's':
                value = strtol(optarg, (char**)NULL, 10);
                if (not value or (1 > value) or (10 < value)) {
                    HelpMessage();
                }
                __global_args._speed = value;
                break;
            case 'H':
                value = strtol(optarg, (char**)NULL, 10);
                if (not value or (2 > value) or (4 < value)) {
                    HelpMessage();
                }
                __global_args._level_height = value;
                break;
            case 't':
                value = strtol(optarg, (char**)NULL, 10);
                if (not value or (100 > value) or (10000 < value)) {
                    HelpMessage();
                }
                __global_args._door_timeout = value;
                break;

            case 'h':
            case '?':
                HelpMessage();
                break;

            default: break;
        }
        opt = getopt(argc_, argv_, __opt_string);
    }
    PLift lift = std::make_shared<Lift>(__global_args._levels,
                                        __global_args._speed,       
                                        __global_args._level_height,
                                        __global_args._door_timeout);
    /// Запуск лифта.
    lift->start();
    /// Ожидание системного сигнала завершения работы.
    PSignalDispatcher sid_disp = std::make_shared<SignalDispatcher>([] {
        LOG(DEBUG) << "Press Enter to terminate.";
    }, std::bind(&Lift::stop, lift.get()));

    return EXIT_SUCCESS;
}
