/** Copyright &copy; 2017, rostislav.vel@gmail.com.
 * \brief  Класс реализующий обслуживание команд.
 * \author Величко Ростислав
 * \date   14.11.2017
 */

#include <atomic>
#include <mutex>
#include <thread>
#include <memory>


#include "Timer.hpp"

#define LOADING_TIMEOUT_MIN 1000 /// Эмуляция min задерки пасажира перед нажатием на кнопку этажа.
#define LOADING_TIMEOUT_MAX 5000 /// Эмуляция max задерки пасажира перед нажатием на кнопку этажа.
#define OPENING_TIMEOUT 5000 /// Обязательная задежка после открытия двери перед обратным закрытием.


typedef std::mutex Mutex;
typedef std::atomic_bool ABool;
typedef std::thread Thread;
typedef std::shared_ptr<Thread> PThread;
typedef utils::Timer Timer;
typedef std::shared_ptr<Timer> PTimer;


/**
 * \brief По умолчанию лифт начинает свою работу с первого этажа. Дверь в состоянии покоя закрыта. 
 *        После ввода этажа эмулируется сценарий: 
 *        Открывается дверь -> ожидание LOADING_TIMEOUT_(random)  -> нажимается кнопка этажа -> дверь закрывается -> 
 *        следует до заданного этажа -> дверь открывается -> ожидание UNLOADING_TIMEOUT -> дверь закрывается.
 */
class Lift { 
    const int _levels;
    const int _speed;
    const int _level_height;
    const int _door_timeout;
    
    Mutex _m;
    ABool _is_run;
    PThread _thread;

    int _cur_level; /// Текущий этажа.

    PTimer _timeout; /// Таймер симуляции задержек операций.
    
    /**
     * \brief Метод выполняет запуск функций лифта в собственно потоке.
     */
    void run();

    /**
     * \brief Метод выполняет процесс открытия двери.
     */
    void movToFirstLevel();

    /**
     * \brief Метод выполняет процесс открытия двери.
     */
    void openDoor();

    /**
     * \brief Метод выполняет эмуляцию входа пассажира в лифт.
     */
    void loadingTimeout();

    /**
     * \brief Метод выполняет эмуляцию нажатия на кнопку этажа (эмуляция действий).
     */
    void pushLevelButton();

    /**
     * \brief Метод выполняет функцию ожидания пока пассажир выйдет из лифта.
     */
    void openingTimeout();

    /**
     * \brief Метод выполняет процесс закрытия двери.
     */
    void closeDoor();

    /**
     * \brief Метод выполняет процесс до заданного этажа.
     * \param need_level_ Требуемый этаж.
     */
    void movToLevel(int need_level_);

public:
    explicit Lift(int levels_, int speed_, int level_height_, int door_timeout_);

    void start();
    void stop();
};
